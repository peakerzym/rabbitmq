import { Body, Controller, Post } from "@nestjs/common";
import { BullLimitTickeService } from "./bulls.service";

@Controller('bull')
export class BullLimitTickeController {
  constructor(
    private  readonly bullLimitTickeService: BullLimitTickeService,
  ){}

  @Post('check-ticket')
  async checkTicket (@Body() body : any) {
    return this.bullLimitTickeService.checkTicket(body)
  }

  @Post('payment')
  async payment (@Body() body : any) {
    return await Promise.all([
      this.bullLimitTickeService.payment(body),
      this.bullLimitTickeService.payment(body),
      this.bullLimitTickeService.payment(body),
      this.bullLimitTickeService.payment(body),
      this.bullLimitTickeService.payment(body)
    ])
  }
}