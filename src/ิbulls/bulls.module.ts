import { CacheModule } from "@nestjs/cache-manager";
import { Module } from "@nestjs/common";
import { RedisOptions } from "src/redis.options";
import { BullLimitTickeController } from "./bulls.controller";
import { BullLimitTickeService } from "./bulls.service";
import { TypegooseModule } from "nestjs-typegoose";
import { CheckTickets } from "src/models/checkTicket.model";
import { CheckWrites } from "src/models/checkWrites.model";
import { Tickets } from "src/models/tickets.model";

@Module({
  imports: [
    CacheModule.registerAsync(RedisOptions),
    TypegooseModule.forFeature([
      Tickets,
      CheckWrites,
      CheckTickets
    ]),
  ],

  controllers: [BullLimitTickeController],
  providers: [BullLimitTickeService],
})
export class BullLimitTicketModule {}