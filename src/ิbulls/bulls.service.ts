import { Injectable } from "@nestjs/common";
import { RedisStore } from "cache-manager-redis-store";
import * as iQueue from 'bull';
import { Job, Queue } from 'bull';
import { ReturnModelType } from "@typegoose/typegoose";
import { InjectModel } from "nestjs-typegoose";
import { CheckTickets } from "src/models/checkTicket.model";

@Injectable()
export class BullLimitTickeService {

  
  constructor(
    @InjectModel(CheckTickets) 
    private readonly checkTicketsModel: ReturnModelType<typeof CheckTickets>,
  ){

  }

  async delay(delayInms:number)  {
    return new Promise(resolve => setTimeout(resolve, delayInms));
  }

  async checkTicket(body:any) {
    
  }

  async payment(body:any) {

    let accId = Math.random().toString()
    if(body.accountId!=="" && body.accountId!==undefined){
      accId = body.accountId
    }

    const queue = new iQueue(
      body.eventId,
      'redis://127.0.0.1:8879',
      {
        limiter: {
          max: 1,
          duration: 2000,
        }
      }
    )

    const res = await queue.add(`${body.eventId}`,
      {
        eventid:body.eventId,
        accountId:accId, 
        paxes: body.paxes
      },{
        lifo: false
      }
      
    )

    let waitingCount = await queue.getWaitingCount()
    console.log('waitingCoun', waitingCount )
    queue.process(`${body.eventId}`, async (job:Job, done) => {
      
      await this.delay(5000)
      await this.checkLimitTicket(accId, body.eventId, body.paxes)
      .then((data)=>{
        done(null, data)
      }).catch((err)=>{
        done(new Error(err))
      })
    
    });
    

    queue.on('waiting', function (jobId) {
      console.log(`Waiting ${jobId}`);
    });

    queue.on('active', function (job, jobPromise) {
      console.log(`start ${job.name} ${job.id}`);
    })
    queue.on('progress', function (job, progress) {
      console.log(`Progress ${job.id} ${progress}`);
    })

    queue.on('failed', function (job, err) {
      console.log(`failed ${job.id} ${err}`);
    })

    queue.on('completed', (job, result) => {
      console.log(`Job ${job.name} ${job.id} completed with result ${result}`);
    })

    return await res.finished()


  }

  async checkLimitTicket(accountId:string, eventId:string, paxes:any){
    

    let canStamp = true
    for(let pax of paxes){

      let numCheckTickets = await this.checkTicketsModel.findOne({
        event_id:eventId,
        ticket_id:pax.ticketId,
        status:{$eq:""}
      }).countDocuments()
      
      if(pax.numberOfPax > numCheckTickets ){
        if(canStamp) canStamp = false
        break;
      }
    }

    if(canStamp){
      
      for(let pax of paxes){
        
        for(let i=0; i<pax.numberOfPax; ++i){
          
          await this.checkTicketsModel.updateOne({  
            event_id:eventId,
            ticket_id:pax.ticketId,
            status:{$eq:""}
          },{ 
            account_id:accountId,
            status:"reserve"
          })
        }
     
      }
    }

    return canStamp

  }
  /*async checkTicket(body:any) {

    const queue = new iQueue(
      body.eventId,
      'redis://127.0.0.1:8879',
      {
        limiter: {
          max: 1,
          duration: 3000
        }
      }
    )

    const res = await queue.add(`${body.eventId}`,
      {
        eventid:body.eventId,
        firstname:'suttipong', 
        lastname: 'buahom'
      }
    )

    queue.process(`${body.eventId}`, async (job:Job, done) => {
      
     

      done(null, job.data)

    
    });


  }*/


}