import { Injectable, OnModuleInit, Logger } from '@nestjs/common';
import amqp, { ChannelWrapper } from 'amqp-connection-manager';
import { ConfirmChannel } from 'amqplib';
import { RbmqService } from './rbmq.service';
//import { EmailService } from 'src/email/email.service';

@Injectable()
export class ConsumerService implements OnModuleInit {
  private channelWrapper: ChannelWrapper;
  private channelWrapper2: ChannelWrapper;
  private readonly logger = new Logger(ConsumerService.name);
  constructor(
    private readonly rbmqService: RbmqService,
  ) {
    const connection = amqp.connect(['amqp://localhost:5672']);
    this.channelWrapper = connection.createChannel();
    this.channelWrapper2 = connection.createChannel();
  }

  async delay(delayInms:number)  {
    return new Promise(resolve => setTimeout(resolve, delayInms));
  }

  public async onModuleInit() {
    try {
      await this.channelWrapper.addSetup(async (channel: ConfirmChannel) => {
        channel.prefetch(1);
        await channel.assertQueue('checkConsumer', { durable: true });
        await channel.consume('checkConsumer', async (message) => {
          if (message) {

            const content = JSON.parse(message.content.toString());
            const eventId = content.eventId
            console.log(eventId)
            
            try {
              
              await this.channelWrapper.checkQueue(eventId)
              
              //console.log(checkQueue)

            } catch (error) {
              

              /*await this.channelWrapper.addSetup(async (channel: ConfirmChannel) => {
        
                channel.prefetch(1);
        
                await channel.assertQueue(eventId, { durable: true});
                await channel.consume(eventId, async (message) => {
                  if (message) {
                    await this.delay(10000)

                    const content = JSON.parse(message.content.toString());
                    this.logger.debug('Received message:', content);
                    
                    console.log('finish delay')
                    channel.ack(message);
                  }
                },{ });
              });*/
              

              console.log(error)
            }

            await this.delay(3000)


            //const content = JSON.parse(message.content.toString());
            //this.logger.log('Received message:', content);
            //await this.emailService.sendEmail(content);
            //await this.delay(1000)
            //await this.rbmqService.check(content)
            //console.log('finish delay '+ new Date())

            channel.ack(message);
          }
        });
      });
      this.logger.log('Consumer service started and listening for messages.');
    } catch (err) {
      this.logger.error('Error starting the consumer:', err);
    }
  }

 
}