import { Injectable, OnModuleInit, Logger } from '@nestjs/common';
import amqp, { ChannelWrapper } from 'amqp-connection-manager';
import { ConfirmChannel } from 'amqplib';
import { RbmqService } from './rbmq.service';
//import { EmailService } from 'src/email/email.service';

@Injectable()
export class ConsumerService2 implements OnModuleInit {
  private channelWrapper: ChannelWrapper;
  private readonly logger = new Logger(ConsumerService2.name);
  constructor(
    private readonly rbmqService: RbmqService,
  ) {
    const connection = amqp.connect(['amqp://localhost:5672']);
    this.channelWrapper = connection.createChannel();
  }

  async delay(delayInms:number)  {
    return new Promise(resolve => setTimeout(resolve, delayInms));
  }

  public async onModuleInit() {
    try {
      await this.channelWrapper.addSetup(async (channel: ConfirmChannel) => {
        channel.prefetch(1);
        await channel.assertQueue('ticketQueue', { durable: true });
        //this.channel.exc
        await channel.consume('ticketQueue', async (message) => {
          if (message) {
            const content = JSON.parse(message.content.toString());
            this.logger.log('Received message:', content);
            //await this.emailService.sendEmail(content);
            await this.rbmqService.check(content)
            await this.delay(5000)
            channel.ack(message);
          }
        });
      });
      this.logger.log('Consumer service started and listening for messages.');
    } catch (err) {
      this.logger.error('Error starting the consumer:', err);
    }
  }
}