import { Body, Controller, Delete, Get, Post } from '@nestjs/common';
import { RbmqService } from './rbmq.service';

@Controller('rabbitmq')
export class RbmqController {
  
  constructor(
    private readonly rbmqService: RbmqService,
  ) {}

  @Get('test')
  async test () {

    return await Promise.all([
      this.rbmqService.test(1),
      this.rbmqService.test(2),
      this.rbmqService.test(3),
      this.rbmqService.test(4),
      this.rbmqService.test(5),
    ])
  }


  @Delete('ticket')
  async delTicket (@Body() body : any) {
    return await this.rbmqService.deleteTicket(body)
  }

  @Post('ticket')
  async createTicket (@Body() body : any) {
    return await this.rbmqService.createTicket(body)
  }

  @Post('order')
  async order (@Body() body : any) {
    return await Promise.all([
      this.rbmqService.order(body),
      this.rbmqService.order(body),
      /*this.rbmqService.order(body),
      this.rbmqService.order(body),
      this.rbmqService.order(body),
      this.rbmqService.order(body),
      this.rbmqService.order(body),
      this.rbmqService.order(body),
      this.rbmqService.order(body),
      this.rbmqService.order(body),*/

    ])
  }

  @Post('create-queue')
  async createQueueFrom (@Body() body : any) {
    return await this.rbmqService.createQueueFrom(body)
  }

}
