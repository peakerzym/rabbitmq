import { Module } from '@nestjs/common';
import { RbmqService } from './rbmq.service';
import { RbmqController } from './rbmq.controller';
import { ProducerService } from './producer.service';
import { ConsumerService } from './consumer.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { Tickets } from 'src/models/tickets.model';
import { ConsumerService2 } from './consumer2.service';
import { CheckWrites } from 'src/models/checkWrites.model';
import { CheckTickets } from 'src/models/checkTicket.model';

@Module({
  imports: [
    TypegooseModule.forFeature([
      Tickets,
      CheckWrites,
      CheckTickets
    ]),
  ],
  providers: [RbmqService, ProducerService, ConsumerService],
  controllers: [RbmqController],
  exports: [ProducerService, ConsumerService]
})
export class RbmqModule {}
