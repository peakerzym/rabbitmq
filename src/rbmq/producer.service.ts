import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import amqp, { ChannelWrapper } from 'amqp-connection-manager';
import { Channel, ConfirmChannel } from 'amqplib';
import { RbmqService } from './rbmq.service';

@Injectable()
export class ProducerService {
  private channelWrapper: ChannelWrapper;
  private readonly logger = new Logger(ProducerService.name);
  constructor(
    //private readonly rbmqService: RbmqService,
  ) {
    const connection = amqp.connect(['amqp://localhost:5672']);

    this.channelWrapper = connection.createChannel()
    /*this.channelWrapper = connection.createChannel({
      setup: (channel: Channel) => {
        return channel.assertQueue('ticketQueue', { durable: true, maxLength:1 });
      },
    });*/
  }

  async addCheckConsumer(eventId:string) {
    try {
      await this.channelWrapper.sendToQueue(
        "checkConsumer",
        Buffer.from(JSON.stringify({eventId:eventId})),
        {
          persistent: true
        },
        function(err, ok) {
          if (err !== null)
            console.warn('Message nacked!');
          else
            console.log('-' +'Message acked '+' '+ok);
        }
      )
      Logger.debug('Sent To Queue');
    } catch (error) {
      throw new HttpException(
        'Error adding mail to queue',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async addTicketQueue(eventId:string, data: any) {
    try {
      await this.channelWrapper.sendToQueue(
        eventId,
        Buffer.from(JSON.stringify(data)),
        {
          persistent: true
        }/*,
        function(err, ok) {
          if (err !== null)
            console.warn('Message nacked!');
          else
            console.log('-' +'Message acked '+' '+ok);
        }*/
      )
      Logger.debug('Sent To Queue');
    } catch (error) {
      throw new HttpException(
        'Error adding mail to queue',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async delay(delayInms:number)  {
    return new Promise(resolve => setTimeout(resolve, delayInms));
  }

  /*async addTicketQueue (data: any){
    try {
      await this.channelWrapper.sendToQueue(
        'ticketQueue',
        Buffer.from(JSON.stringify(data)),
        {
          persistent: true,
        },
        function(err, ok) {
          if (err !== null)
            console.warn('Message nacked!');
          else
            console.log(err + '-' +'Message acked');
        }
      )
      Logger.log('Sent To Queue');



      await this.channelWrapper.addSetup(async (channel: ConfirmChannel) => {
        channel.prefetch(1);
        await channel.assertQueue('ticketQueue', { durable: true });
        //this.channel.exc
        await channel.consume('ticketQueue', async (message) => {
          if (message) {
            const content = JSON.parse(message.content.toString());
            this.logger.log('Received message:', content);
            //await this.emailService.sendEmail(content);
            await this.rbmqService.check(content)
            await this.delay(5000)
            channel.ack(message);
          }
        });
      });



    } catch (error) {
      throw new HttpException(
        'Error adding mail to queue',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }*/

  
}