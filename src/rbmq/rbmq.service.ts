import { Injectable, Logger } from '@nestjs/common';
import { ProducerService } from './producer.service';
import { InjectModel } from 'nestjs-typegoose';
import { ConfigTickets, Tickets } from 'src/models/tickets.model';
import { ReturnModelType } from '@typegoose/typegoose';
import amqp, { ChannelWrapper, Channel } from 'amqp-connection-manager';
import * as iQueue from 'bull';
import { ConfirmChannel } from 'amqplib';
import { CheckWrites } from 'src/models/checkWrites.model';
import { CheckTickets } from 'src/models/checkTicket.model';
import { ConsumerService } from './consumer.service';
import { Job, Queue } from 'bull';

@Injectable()
export class RbmqService {
  private readonly logger = new Logger(RbmqService.name);


  private channelWrapper: ChannelWrapper;
  
  constructor(
    private producerService: ProducerService,
    @InjectModel(Tickets) 
    private readonly ticketsModel: ReturnModelType<typeof Tickets>,
    @InjectModel(CheckTickets) 
    private readonly checkTickets: ReturnModelType<typeof CheckTickets>,
    @InjectModel(CheckWrites) 
    private readonly checkWritesModel: ReturnModelType<typeof CheckWrites>,
    
  ) {
    const connection = amqp.connect(['amqp://localhost:5672']);
    this.channelWrapper = connection.createChannel();
  }
  
  async test(n:number){
    let id = Math.random().toString()
    const d = n +' - '+ id + ' - ' + new Date().getTime()
    //await this.producerService.addToEmailQueue(d);
    return d;
  }

  async deleteTicket(body:any){
    return await this.ticketsModel.deleteOne({
      event_id:body.eventId
   })
  }

  async createTicket(body:any){

    let dataTickets = await this.ticketsModel.findOne({
      event_id:body.eventId
    }) as Tickets

    if(dataTickets){

      const tickets = body.tickets.map(val => {
        const tk = new ConfigTickets()
        tk.ticket_id = val.ticketId
        tk.limit = val.limit
        tk.limit_per_person = val.limitPerPerson
        tk.count_ticket = 0
        return tk
      })

      await this.ticketsModel.updateOne(
        { event_id:body.eventId },
        { 
          max: body.limit,
          start_date:body.startDate,
          end_date:body.endDate,
          updatedAt:new Date(),
          limit_per_person:body.limitPerPerson,
          tickets:tickets
        }

      ).orFail(() => Error('Fail')).exec()
    }else{
      const createData = new Tickets()
      createData.event_id = body.eventId
      createData.start_date = body.startDate
      createData.end_date = body.endDate
      createData.limit_per_person=body.limitPerPerson

      const tickets = body.tickets.map(val => {
        const tk = new ConfigTickets()
        tk.ticket_id = val.ticketId
        tk.limit = val.limit
        tk.limit_per_person = val.limitPerPerson
        tk.count_ticket = 0
        return tk
      })
      createData.tickets = tickets

      dataTickets = await this.ticketsModel.create(createData)
    }

    for(let val of body.tickets){
      const ck = new CheckTickets ()
      ck.account_id = ""
      ck.status = ""
      ck.event_id = body.eventId
      ck.ticket_id = val.ticketId
      for(let i =0; i< val.limit; ++i){
        console.log(5)
        await this.checkTickets.create(ck)
      }
    }


    return dataTickets
  }

  async order(body:any) {

    let id = 'acc'+Math.random()

    for(let pax of body.paxes) {
      const setData = {
        dateIn:new Date().getTime(),
        accountId:id,
        eventId:body.eventId,
        ticketId:pax.ticketId,
        numberOfPax:pax.numberOfPax
      }

      await this.producerService.addCheckConsumer(body.eventId)


     
      /*try {
        const checkQueue = await this.channelWrapper.checkQueue(body.eventId)
        if(checkQueue.consumerCount===0){
          this.channelWrapper.deleteQueue(body.eventId)
          await this.queueCreateConsumer(body.eventId)
        }else{
          await this.producerService.addTicketQueue(body.eventId, setData)
        }
        

      } catch (error) {

        await this.queueCreateConsumer(body.eventId)

        console.log('root-error')
      }*/


      //await this.createQueue(body.eventId)

      //await this.producerService.addTicketQueue("ticketQueue", setData)
      //await this.producerService.addTicketQueue(body.eventId, setData)
      

      //await this.createQueue(body.eventId)

    }


    const d = ' - '+ id + ' - ' + new Date().getTime()
    //await this.producerService.addToEmailQueue(d)
    
    
    return d;

  }

  public async queueCreateConsumer(eventId) {
    
    const queue = new iQueue(
      eventId,
      'redis://127.0.0.1:8879',
      {
        limiter: {
          max: 1,
          duration: 6000,
        }
      }
    )

    const res = await queue.add(`${eventId}`,
      {
        eventid:eventId
      },{
        lifo: false
      }
    )

    queue.process(`${eventId}`, async (job:Job, done) => {

      try {
    

        const checkQueue = await this.channelWrapper.checkQueue(eventId)
        console.log(checkQueue)
        
        done(null, job.data)
      } catch (error) {
        await this.createQueue(eventId)
        done(null, job.data)

        console.log('queueCreateConsumer - error')
      }
     

    })

    queue.on('completed', (job, result) => {
      console.log(`Job ${job.name} ${job.id} completed with result ${result}`);
    })

    await res.finished()

  }


  public async createQueueFrom(body:any) {
    await this.createQueue(body.eventId)
  }

  public async createQueue(eventId) {
    try {

      await this.channelWrapper.addSetup(async (channel: ConfirmChannel) => {
        
        channel.prefetch(1);


        /*
         exclusive?: boolean | undefined;
        durable?: boolean | undefined;
        autoDelete?: boolean | undefined;
        arguments?: any;
        messageTtl?: number | undefined;
        expires?: number | undefined;
        deadLetterExchange?: string | undefined;
        deadLetterRoutingKey?: string | undefined;
        maxLength?: number | undefined;
        maxPriority?: number | undefined;*/

        await channel.assertQueue(eventId, { durable: true});
        await channel.consume(eventId, async (message) => {
          if (message) {
            await this.delay(3000)
            const content = JSON.parse(message.content.toString());
            this.logger.debug('Received message:', content);
            await this.check(content)
            
            console.log('finish delay')
            channel.ack(message);
          }
        },{ });
      });
      this.logger.debug('Consumer service started and listening for messages.');
    } catch (err) {
      this.logger.error('Error starting the consumer:', err);
    }
  }



  async delay(delayInms:number)  {
    return new Promise(resolve => setTimeout(resolve, delayInms));
  }


  async check (data:any){

    

    const cw = new CheckWrites()
    cw.accountId = data.accountId
    await this.checkWritesModel.create(cw)

    console.log(data.accountId +' - ' + new Date().getTime())
  }




}
