import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RbmqModule } from './rbmq/rbmq.module';
import { TypegooseModule } from 'nestjs-typegoose';
import { BullLimitTicketModule } from './ิbulls/bulls.module';

@Module({
  imports: [
    TypegooseModule.forRootAsync({
      imports: [],
      useFactory: async () => ({
        uri: 'mongodb+srv://peakerzym:tGBiOAkRGANCbVf0@peakerzym.ri1wmmb.mongodb.net/rabbitmq',
      }),
      inject: []
    }),
    RbmqModule,
    BullLimitTicketModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
