import { Severity, modelOptions, prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";
@modelOptions({options: {customName: "check_writes", allowMixed: Severity.ALLOW}})
export class CheckWrites extends TimeStamps {
  @prop({ required: true })
  accountId:string
}