import { Severity, modelOptions, prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";

@modelOptions({ 
  schemaOptions: { 
    timestamps: true,
    versionKey: false
  }
})

export class ConfigTickets {

  @prop({ required: true })
  ticket_id:string

  @prop({ required: true })
  limit:number

  @prop({ required: true })
  limit_per_person:number
  
  @prop({ required: true })
  count_ticket:number

}
@modelOptions({options: {customName: "tickets", allowMixed: Severity.ALLOW}})
export class Tickets extends TimeStamps {

  @prop({ required: true })
  event_id:string
  
  @prop({ required: true })
  start_date:Date

  @prop({ required: true })
  end_date:Date

  @prop({ required: true })
  limit_per_person:number

  @prop({ required: true })
  tickets:Array<ConfigTickets>
}