import { modelOptions, Severity, prop } from "@typegoose/typegoose"
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses"

@modelOptions({ 
  schemaOptions: { 
    timestamps: true,
    versionKey: false
  }
})

@modelOptions({options: {customName: "check_tickets", allowMixed: Severity.ALLOW}})
export class CheckTickets extends TimeStamps {
  
  @prop({ required: false })
  account_id:string

  @prop({ required: false })
  status:string

  @prop({ required: true })
  event_id: string

  @prop({ required: true })
  ticket_id: string

}