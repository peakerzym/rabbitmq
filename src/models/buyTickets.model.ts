import { Severity, modelOptions, prop } from "@typegoose/typegoose"
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses"

@modelOptions({ 
  schemaOptions: { 
    timestamps: true,
    versionKey: false
  }
})

@modelOptions({options: {customName: "buy_tickets", allowMixed: Severity.ALLOW}})
export class BuyTickets extends TimeStamps {
  
  @prop({ required: true })
  account_id:string

  @prop({ required: true })
  status:string

  @prop({ required: true })
  eventid: string

}